using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TextUtility
{
    public static void WriteDataToFile(string path, string jsonString)
    {
        File.WriteAllText(path, jsonString);
    }

    public static string ReadFile(string fileName)
    {
        string folderPath = Application.persistentDataPath;
        string filePath = folderPath + "/" + fileName;
        string result = "";
        if (!Directory.Exists(folderPath))
        {
            result = "";
        }
        if (!File.Exists(filePath))
        {
            result = "";
        }
        else
        {
            result = File.ReadAllText(filePath);
        }

        return result;
    }
}
