using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SFB;

public class FileLoader : MonoBehaviour
{
    public BattleSimulation _battleSimulation;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenFile()
    {
        var extensions = new[] { new ExtensionFilter("Text Files", "json", "txt" ) };
        var paths = StandaloneFileBrowser.OpenFilePanel("Open File", "", extensions, false);

        if (paths.Length > 0)
        {
            StartCoroutine(OutputCoroutine(new System.Uri(paths[0]).AbsoluteUri));
        }
    }

    private IEnumerator OutputCoroutine(string url)
    {
        var loader = new WWW(url);
        yield return loader;
        _battleSimulation.StartSimulation(loader.text);
    }
}
