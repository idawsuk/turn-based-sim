using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSimulation : MonoBehaviour
{
    public BattleActor TeamLeft;
    public BattleActor TeamRight;

    BattleController _battleController;
    BattleEventRecorder _battleEventRecorder;

    // Start is called before the first frame update
    void Start()
    {
        _battleController = new BattleController();
        _battleController.OnBattleFinished += OnBattleFinished;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartSimulation(string json)
    {
        _battleEventRecorder = JsonUtility.FromJson<BattleEventRecorder>(json);

        List<BattleHero> heroes = new List<BattleHero>();
        heroes.AddRange(TeamLeft.Heroes);
        heroes.AddRange(TeamRight.Heroes);
        _battleController.Initialization(heroes);

        for (int i = 0; i < _battleEventRecorder.BattleActions.Count; i++)
        {
            _battleController.AddAction(_battleEventRecorder.BattleActions[i]);
        }

        _battleController.CheckTeamHealthPoint();
    }

    void OnBattleFinished(TeamSide winner)
    {
        Debug.LogFormat("{0} Win!", winner);

        if (winner != _battleEventRecorder.Winner)
            Debug.Log("Verification failed!");
    }
}
