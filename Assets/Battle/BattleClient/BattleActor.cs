using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleActor : MonoBehaviour
{
    public List<BattleHero> Heroes;
    public List<HeroFrontend> HeroPositionIndexes;

    BattleTurnController _battleTurnController;

    public delegate void SelectedHero(Transform transform);
    public SelectedHero OnHeroSelected;

    // Start is called before the first frame update
    void Start()
    {
        _battleTurnController = FindObjectOfType<BattleTurnController>();

        for (int i = 0; i < HeroPositionIndexes.Count; i++)
        {
            HeroPositionIndexes[i].OnSelected += HeroSelected;
        }

        for (int i = 0; i < Heroes.Count; i++)
        {
            Heroes[i].OnDefeated += OnHeroDefeated;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSelectionActive(BattleHero Hero, bool selectionActive)
    {
        for (int i = 0; i < HeroPositionIndexes.Count; i++)
        {
            if (HeroPositionIndexes[i].PositionIndex.Equals(Hero.Position))
            {
                HeroPositionIndexes[i].Selectable(selectionActive);
                HeroPositionIndexes[i].SetRenderer(selectionActive);
            }
        }
    }

    public void SetSelectionActive(bool selectionActive)
    {
        for (int i = 0; i < HeroPositionIndexes.Count; i++)
        {
            HeroPositionIndexes[i].Selectable(selectionActive);
            HeroPositionIndexes[i].SetRenderer(selectionActive);
        }
    }

    void HeroSelected(int positionIndex)
    {
        BattleHero selectedHero = null;
        for (int i = 0; i < Heroes.Count; i++)
        {
            if (Heroes[i].Position == positionIndex)
            {
                selectedHero = Heroes[i];
                OnHeroSelected?.Invoke(HeroPositionIndexes[i].transform);
                break;
            }
        }

        if(selectedHero != null)
        {
            _battleTurnController.OnHeroSelected(selectedHero);
        }
    }

    public void SelectHero(BattleHero hero)
    {
        for (int i = 0; i < HeroPositionIndexes.Count; i++)
        {
            if (HeroPositionIndexes[i].PositionIndex == hero.Position)
            {
                OnHeroSelected?.Invoke(HeroPositionIndexes[i].transform);
                break;
            }
        }

        _battleTurnController.OnHeroSelected(hero);
    }

    void OnHeroDefeated(BattleHero battleHero)
    {
        SetSelectionActive(battleHero, false);
    }
}
