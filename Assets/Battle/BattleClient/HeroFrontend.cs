using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroFrontend : MonoBehaviour
{
    public int PositionIndex;
    public SpriteRenderer Sprite;

    Selectable _selectable;

    public delegate void Selected(int positionIndex);
    public Selected OnSelected;

    // Start is called before the first frame update
    void Awake()
    {
        _selectable = GetComponent<Selectable>();
        _selectable.OnSelected += () =>
        {
            OnSelected?.Invoke(PositionIndex);
        };
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Selectable(bool enable)
    {
        _selectable.enabled = enable;
    }

    public void SetRenderer(bool active)
    {
        Color color = Sprite.color;
        color.a = active ? 1 : .2f;

        Sprite.color = color;
    }
}
