using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionView : MonoBehaviour
{
    public BattleActor BattleActor;
    public Transform Selection;
    BattleTurnController _battleTurnController;

    public GameObject ActionButtons;

    // Start is called before the first frame update
    void Start()
    {
        _battleTurnController = FindObjectOfType<BattleTurnController>();
        _battleTurnController.OnFinishSelection += OnFinishSelection;
        BattleActor.OnHeroSelected += OnHeroSelected;
        Selection.gameObject.SetActive(false);
        if(ActionButtons) ActionButtons.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnHeroSelected(Transform transform)
    {
        Selection.transform.position = transform.position;
        Selection.gameObject.SetActive(true);
        if (ActionButtons) ActionButtons.SetActive(true);
    }

    void OnFinishSelection()
    {
        Selection.gameObject.SetActive(false);
        if (ActionButtons) ActionButtons.SetActive(false);
    }
}
