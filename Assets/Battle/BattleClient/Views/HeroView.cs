using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HeroView : MonoBehaviour
{
    public BattleActor BattleActor;
    public HeroFrontend HeroFrontend;
    public TextMeshProUGUI DisplayHP;

    BattleHero _hero;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < BattleActor.Heroes.Count; i++)
        {
            if(BattleActor.Heroes[i].Position == HeroFrontend.PositionIndex)
            {
                _hero = BattleActor.Heroes[i];
                break;
            }
        }

        _hero.OnTakeDamage += OnTakeDamage;
        DisplayHP.text = _hero.CurrentHP.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTakeDamage(int currentHP)
    {
        DisplayHP.text = currentHP.ToString();
    }
}
