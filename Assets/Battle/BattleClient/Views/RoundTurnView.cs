using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RoundTurnView : MonoBehaviour
{
    public TextMeshProUGUI TurnText;
    BattleTurnController _battleTurnController;

    // Start is called before the first frame update
    void Start()
    {
        _battleTurnController = FindObjectOfType<BattleTurnController>();
        _battleTurnController.OnChangeTurnEvent += OnTurnChanged;
        _battleTurnController.OnBattleFinishedEvent += OnBattleFinished;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTurnChanged(TeamSide teamSide)
    {
        TurnText.text = teamSide.ToString();
    }

    void OnBattleFinished(TeamSide winner)
    {
        TurnText.text = winner.ToString() + " win!";
    }
}
