using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoBattle : MonoBehaviour
{
    public BattleActor MyTeam;
    public BattleActor OpponentTeam;

    public TeamSide MySide;

    BattleTurnController _battleTurnController;

    // Start is called before the first frame update
    void Start()
    {
        _battleTurnController = FindObjectOfType<BattleTurnController>();

        if(_battleTurnController != null)
            _battleTurnController.OnChangeTurnEvent += OnChangeTurnEventTriggered;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnChangeTurnEventTriggered(TeamSide currentTurn)
    {
        List<int> availableOpponent = new List<int>();

        for (int i = 0; i < OpponentTeam.Heroes.Count; i++)
        {
            BattleHero hero = OpponentTeam.Heroes[i];
            if (hero.CurrentHP > 0)
            {
                int position = OpponentTeam.HeroPositionIndexes[i].PositionIndex;
                availableOpponent.Add(position);
            }
        }

        if (currentTurn.Equals(MySide))
        {
            StartCoroutine(TurnStarted(availableOpponent));
        }

        //finish action
    }

    IEnumerator TurnStarted(List<int> availableOpponent)
    {
        for (int i = 0; i < MyTeam.Heroes.Count; i++)
        {
            BattleHero hero = MyTeam.Heroes[i];
            if (hero.CurrentHP > 0)
            {
                yield return new WaitForSeconds(1);
                MyTeam.SelectHero(hero);

                yield return new WaitForSeconds(1);
                _battleTurnController.QueueAction(BattleActionType.Attack);

                NumberGenerator numberGenerator = new NumberGenerator((int)System.DateTime.Now.Ticks);
                int randomPosition = numberGenerator.Generate(0, availableOpponent.Count);
                int enemyPosition = availableOpponent[randomPosition];

                yield return new WaitForSeconds(1);
                _battleTurnController.OnHeroSelected(OpponentTeam.Heroes[enemyPosition]);
            }
        }
    }
}
