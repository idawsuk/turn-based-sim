using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SFB;

public class BattleTurnController : MonoBehaviour
{
    public BattleActor TeamLeft;
    public BattleActor TeamRight;

    BattleController _battleController;

    List<BattleHero> _battleHeroes;
    TeamSide _currentTurn = TeamSide.Left;

    BattleHero _selected;
    BattleHero _target;

    BattleEventRecorder _battleRecorder;
    BattleAction _currentAction;
    Queue<BattleAction> _currentTurnActions = new Queue<BattleAction>();

    public delegate void TeamSideEvent(TeamSide currentTurn);
    public TeamSideEvent OnChangeTurnEvent;
    public TeamSideEvent OnBattleFinishedEvent;

    public delegate void Event();
    public Event OnFinishSelection;

    void Start()
    {
        _battleController = new BattleController();
        _battleRecorder = new BattleEventRecorder();
        _battleController.OnBattleFinished += OnBattleFinished;

        StartBattle();
    }

    public void StartBattle()
    {
        List<BattleHero> heroes = new List<BattleHero>();
        heroes.AddRange(TeamLeft.Heroes);
        heroes.AddRange(TeamRight.Heroes);
        _battleController.Initialization(heroes);

        _currentTurn = TeamSide.Right;
        ChangeTurn();
    }

    void StartTurn(TeamSide teamSide)
    {
        _currentTurn = teamSide;
        _battleHeroes = _battleController.GetHeroes(teamSide);
        _currentTurnActions.Clear();

        OnChangeTurnEvent?.Invoke(teamSide);
    }

    void ChangeTurn()
    {
        if (_currentTurn.Equals(TeamSide.Left))
        {
            TeamRight.SetSelectionActive(true);
            TeamLeft.SetSelectionActive(false);
            StartTurn(TeamSide.Right);
        } else
        {
            TeamRight.SetSelectionActive(false);
            TeamLeft.SetSelectionActive(true);
            StartTurn(TeamSide.Left);
        }
    }

    public void OnHeroSelected(BattleHero selected)
    {
        if (_currentTurn == selected.Side)
        {
            _selected = selected;
        }
        else 
        {
            _currentAction.TargetHeroPosition = selected.Position;
            _currentTurnActions.Enqueue(_currentAction);
            _currentAction = null;
            _battleHeroes.Remove(_selected);

            if(_currentTurn.Equals(TeamSide.Left))
            {
                TeamLeft.SetSelectionActive(_selected, false);
                TeamRight.SetSelectionActive(false);
            } else
            {
                TeamRight.SetSelectionActive(_selected, false);
                TeamLeft.SetSelectionActive(false);
            }

            FinishAction();
        }
    }

    public void QueueAction(BattleActionType actionType)
    {
        if (_selected == null)
            return;

        _currentAction = new BattleAction() { ActionType = actionType, HeroPosition = _selected.Position, TimeStamp = (int)System.DateTime.Now.Ticks };

        if (_currentTurn.Equals(TeamSide.Left))
        {
            TeamRight.SetSelectionActive(true);
        }
        else
        {
            TeamLeft.SetSelectionActive(true);
        }
    }

    public void Attack()
    {
        QueueAction(BattleActionType.Attack);
        //if (_selected == null || _target == null)
        //    return;

        //if (_selected.Side.Equals(_currentTurn) && _battleHeroes.Contains(_selected))
        //{
        //    BattleAction action = new BattleAction() { ActionType = BattleActionType.Attack, HeroPosition = _selected.Position, TargetHeroPosition = _target.Position };
        //    _battleRecorder.AddAction(action);
        //    _battleController.AddAction(action);
        //}

        //FinishAction();
    }

    void FinishAction()
    {
        OnFinishSelection?.Invoke();
        _selected = null;
        _target = null;
        if(_battleHeroes.Count <= 0)
        {
            ExecuteActions();
        }
    }

    void ExecuteActions()
    {
        while(_currentTurnActions.Count > 0)
        {
            BattleAction action = _currentTurnActions.Dequeue();
            _battleRecorder.AddAction(action);
            _battleController.AddAction(action);
        }

        ChangeTurn();
        _battleController.CheckTeamHealthPoint();
    }

    void OnBattleFinished(TeamSide winner)
    {
        _battleRecorder.Winner = winner;
        string json = JsonUtility.ToJson(_battleRecorder);

        var path = StandaloneFileBrowser.SaveFilePanel("Save battle file", "", "battleRecord", "json");
        if (!string.IsNullOrEmpty(path))
        {
            TextUtility.WriteDataToFile(path, json);
        }

        OnBattleFinishedEvent?.Invoke(winner);
    }
}
