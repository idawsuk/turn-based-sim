[System.Serializable]
public enum TeamSide
{
    Left, Right
}

public enum BattleActionType
{
    Attack
}