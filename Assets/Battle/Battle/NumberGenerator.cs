using System;

public class NumberGenerator
{
    public int Seed = 1000;

    public NumberGenerator(int seed)
    {
        Seed = seed;
    }

    public int Generate(int min, int max)
    {
        Random rand = new Random(Seed);
        return rand.Next(min, max);
    }

    public double Generate()
    {
        Random rand = new Random(Seed);
        return rand.NextDouble();
    }
}
