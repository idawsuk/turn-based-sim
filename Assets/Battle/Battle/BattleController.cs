using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class BattleController
{
    public List<BattleHero> Heroes;
    public delegate void BattleFinished(TeamSide winner);
    public BattleFinished OnBattleFinished;

    public void Initialization(List<BattleHero> heroes)
    {
        Heroes = heroes;
    }

    public void AddAction(BattleAction action)
    {
        switch (action.ActionType)
        {
            case BattleActionType.Attack:
                AttackHero(action);
                break;
        }
    }

    public void CheckTeamHealthPoint()
    {
        if (IsTeamDefeated(TeamSide.Left))
        {
            OnBattleFinished?.Invoke(TeamSide.Right);
        }
        else if (IsTeamDefeated(TeamSide.Right))
        {
            OnBattleFinished?.Invoke(TeamSide.Left);
        }
    }

    void AttackHero(BattleAction action)
    {
        int attackerPosition = action.HeroPosition;
        int targetPosition = action.TargetHeroPosition;

        BattleHero attacker = Heroes.Find(x => x.Position.Equals(attackerPosition));
        BattleHero target = Heroes.Find(x => x.Position.Equals(targetPosition));

        int multiplier = 1; //2 if critical

        NumberGenerator numGenerator = new NumberGenerator(action.TimeStamp);
        if (numGenerator.Generate(0, 100) > 50) // 50% crit chance
        {
            multiplier = 2;
            UnityEngine.Debug.LogFormat("{0} attack {1} Critical!", attackerPosition, targetPosition);
        }

        int damage = attacker.Hero.Attributes.Attack * multiplier;

        target.TakeDamage(damage);
    }

    bool IsTeamDefeated(TeamSide teamSide)
    {
        List<BattleHero> heroes = GetHeroes(teamSide);
        if(heroes.Count >= 1)
        {
            return false;
        } else
        {
            return true;
        }
    }

    public List<BattleHero> GetHeroes(TeamSide teamSide)
    {
        return Heroes.FindAll(x => x.Side.Equals(teamSide) && x.CurrentHP > 0);
    }
}
