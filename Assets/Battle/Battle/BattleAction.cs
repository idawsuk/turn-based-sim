[System.Serializable]
public class BattleAction
{
    public BattleActionType ActionType;
    public int HeroPosition;
    public int TargetHeroPosition;
    public int TimeStamp;

    public bool Equals(BattleAction otherAction)
    {
        return otherAction.ActionType.Equals(ActionType) && otherAction.HeroPosition.Equals(HeroPosition) && otherAction.TargetHeroPosition.Equals(TargetHeroPosition);
    }
}