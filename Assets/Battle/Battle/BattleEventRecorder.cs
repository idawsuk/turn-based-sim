using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BattleEventRecorder
{
    public List<BattleAction> BattleActions = new List<BattleAction>();
    public TeamSide Winner;

    public void AddAction(BattleAction action)
    {
        BattleActions.Add(action);
    }
}