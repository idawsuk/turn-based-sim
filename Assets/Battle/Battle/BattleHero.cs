[System.Serializable]
public class BattleHero
{
    public Hero Hero;
    public int Position;
    public int CurrentHP;
    public TeamSide Side;
    public delegate void Defeated(BattleHero battleHero);
    public Defeated OnDefeated;
    public delegate void TakeDamageEvent(int currentHP);
    public TakeDamageEvent OnTakeDamage;

    public BattleHero(Hero hero, int position, TeamSide side)
    {
        Hero = hero;
        Position = position;
        CurrentHP = hero.Attributes.Health;
        Side = side;
    }

    public void TakeDamage(int attack)
    {
        int damage = attack / Hero.Attributes.Defense;

        CurrentHP -= damage;

        if (CurrentHP <= 0)
        {
            CurrentHP = 0; //clamp
            OnDefeated?.Invoke(this);
        }

        OnTakeDamage?.Invoke(CurrentHP);
    }

    //  position from Code Atma
    //  ==============
    //  left        right
    //  3   0       6   9
    //  4   1  vs   7   10
    //  5   2       8   11
}
